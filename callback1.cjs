const { rejects } = require('assert');
const { error } = require('console');
const { resolve } = require('path');
const fs = require('fs').promises;

function getDataUsingboardID(findId) {

    return new Promise( (resolve, reject) => {
        fs.readFile('../boards.json', 'utf-8')
        .then( (data) => {
            // convert json data in object
            const boardData = JSON.parse(data);
    
            setTimeout( () => {
                // check the findId is present or not
                // if id present then find method return that object
                const boardId = boardData.find( (currentObj) => {
                    return currentObj.id === findId;
                });
    
                // if the boardId is true
                if(boardId){
                    // resolve the promise with boardId
                    resolve(boardId);
                }
                else{
                    // means the board id is not present
                    reject("Board id is not present");
                }
            }, 2 * 1000);
    
        })
        .catch( (error) => {
            reject(new Error("Error to read boards.json file :", error));
        });
    })
}

// export the getDataUsingboardID function
module.exports = getDataUsingboardID;