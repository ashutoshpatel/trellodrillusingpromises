const { rejects } = require('assert');
const fs = require('fs').promises;

function getAllCardsDataBasedOnListID(findId){

    return new Promise( (resolve, reject) => {
        fs.readFile('../cards.json', 'utf-8')
        .then( (data) => {
            // convert json data in object
            const cardsData = JSON.parse(data);
            // get the all keys of cardsData
            const cardsKey = Object.keys(cardsData);

            setTimeout( () => {
                // check the findId is present or not
                // if id present then find method return that id
                const listId = cardsKey.find( (currentKey) => {
                    return currentKey === findId;
                });

                // if the listId is true
                if(listId){
                    // resolve the promise with cardsData[listId]
                    resolve(cardsData[listId]);
                }
                else{
                    // means the list id is not present in cards.json
                    reject("List id is not present in cards.json");
                }
            }, 2 * 1000);

        })
        .catch( (error) => {
            reject(new Error("Error to read cards.json file :", error));
        });
    });
}

// export the getAllCardsDataBasedOnListID function
module.exports = getAllCardsDataBasedOnListID;