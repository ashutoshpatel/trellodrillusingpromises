const { rejects } = require('assert');
const { error } = require('console');
const fs = require('fs').promises;
const { resolve } = require('path');

function getAllListDataBasesOnBoardID(findId){

    return new Promise( (resolve, reject) => {
        fs.readFile('../lists.json', 'utf-8')
        .then( (data) => {
            // convert json data in object
            const listData = JSON.parse(data);
            // get the all keys of listData
            const keysOfListData = Object.keys(listData);

            setTimeout( () => {
                // check the findId is present or not
                // if id present then find method return that id
                const boardId = keysOfListData.find( (currentKey) => {
                    return currentKey === findId;
                });

                // if the boardId is true
                if(boardId){
                    // resolve the promise with listData[boardId]
                    resolve(listData[boardId]);
                }
                else{
                    // means the board id is not present in lists.json
                    reject("Board id is not present in lists.json");
                }
            }, 2* 1000);

        })
        .catch( (error) => {
            reject(new Error("Error to read lists.json file :", error));
        });
    });
}

// export the getAllListDataBasesOnBoardID function
module.exports = getAllListDataBasesOnBoardID;