const fs = require('fs').promises;
const getDataUsingboardID = require('./callback1.cjs');
const getAllListDataBasesOnBoardID = require('./callback2.cjs');
const getAllCardsDataBasedOnListID = require('./callback3.cjs');
const { error } = require('console');

function problem6(){

    let idOfThanos;
    
    return fs.readFile('../boards.json', 'utf-8')
    .then( (data) => {
        // convert json data in object
        const boardData = JSON.parse(data);

        // Find the board with name 'Thanos'
        const thanosBoard = boardData.find((board) => {
            return board.name === "Thanos";
        });

        // update the idOfThanos
        idOfThanos = thanosBoard.id;

        // call the getDataUsingboardID function to get board data of Thanos
        return getDataUsingboardID(idOfThanos);
    })
    .then( (result) => {
        console.log(result);

        // call the getAllListDataBasesOnBoardID function to get list data of Thanos
        return getAllListDataBasesOnBoardID(idOfThanos);
    })
    .then( (result) => {
        console.log(result);

        // use a forEach loop because we need to get all cards
        result.forEach( (currentObj) => {
            // get the id of current list
            const listId = currentObj.id;

            // call the getAllCardsDataBasedOnListID function to get cards data of current list id
            getAllCardsDataBasedOnListID(listId)
            .then( (result) => {
                console.log(result);
            })
            .catch( (error) => {
                console.log(error);
            });
        });
    })
    .catch( (error) => {
        console.log(error);
    });

}

// export the problem6 function
module.exports = problem6;